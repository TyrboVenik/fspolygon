#include "stdafx.h"
#include "Polygonchik.h"
#include <iostream>
#include <math.h>
#include <time.h>

using namespace std;

Polygon::Polygon() :Len(0){};

Polygon::Polygon(point p) :Len(1)
{
	try {
		mas = new point[1];
	}
	catch (std::bad_alloc& ba) {
		throw "out of memory";
	}

	mas[0] = p;
};

Polygon::Polygon(float x, float y) :Len(1)
{
	try {
		mas = new point[1];
	}
	catch (std::bad_alloc& ba) {
		throw "out of memory";
	}
	mas[0].x = x;
	mas[0].y = y;
}

Polygon::Polygon(int L, const point a[])
{
	try {
		mas = new point[L];
	}
	catch (std::bad_alloc& ba) {
		throw "out of memory";
	}
	Len = L;
	for (int i = 0; i < L; i++)
		mas[i] = a[i];
};


Polygon::~Polygon() 
{
	delete []mas;
}


Polygon::Polygon(Polygon &a):
Len(a.Len)
{
	delete[] mas;
	mas = new point[Len];
	
	for (int i = 0; i < Len; i++)
		mas[i] = a.mas[i];
}

Polygon::Polygon(Polygon &&a) :
	Len(a.Len),mas(a.mas)
{
	a.mas = nullptr;
}


int Polygon::vse_norm(const point& A)const
{
	for (int i = 0; i < Len; i++)
		if ((mas[i].x == A.x) && (mas[i].y == A.y))
			return 0;
	
}

point Polygon::turnPoint(point &A, const float &Alpha)
{
	float x1 = A.x;
	A.x = A.x*cos(Alpha) - A.y*sin(Alpha);
	A.y = x1*sin(Alpha) + A.y*cos(Alpha);
	return A;
}


int Polygon::getLen()const
{
	return Len;
}

point Polygon::getCentre()const
{
	if (Len == 0)
		throw "threre is no polygon";

	point Centr;
	Centr.x = 0;
	Centr.y = 0;
	for (int i = 0; i < Len; i++)
	{
		Centr.x += mas[i].x;
		Centr.y += mas[i].y;
	}
	Centr.x /= Len;
	Centr.y /= Len;
	return Centr;
}


Polygon & Polygon::SetLen(int L)
{
	if (L > Len || L<0)
		throw "incorrect length";
	Len = L;
	return *this;
}

Polygon & Polygon::SetPoint(int i, const point &A)
{
	if (i >= Len || i<0)
		throw "incorect index";
	mas[i] = A;
	return *this;
}

Polygon & Polygon::SetPoint(int i, float x1, float y1)
{
	if (i >= Len || i<0)
		throw "inrorrect index";
	mas[i].x = x1;
	mas[i].y = y1;
	return *this;
}


std::ostream & operator<<(std::ostream& c, const Polygon& A)
{
	c << "Length=" << A.Len << std::endl;
	for (int i = 0; i < A.Len; i++)
		c << "(" << A[i].x << "," << A[i].y << ")" << std::endl;
	return c;
}


point Polygon::operator [](int i)const
{
	if (i<0 || i>Len)
		throw "incorrect index";
	return this->mas[i];
}

Polygon & Polygon::operator()(int Alpha)
{
	if (Alpha % 90 != 0)
		throw "inroccect angle";
	float Alpha1 = 0;
	Alpha1 = Alpha*PI / 180;

	for (int i = 0; i < Len; i++)
		turnPoint(mas[i], Alpha1);

	return *this;
}

Polygon & Polygon::operator()(const point& A)
{
	for (int i = 0; i < Len; i++)
	{
		mas[i].x += A.x;
		mas[i].y += A.y;
	}
	return *this;
}

Polygon & Polygon::operator()(float x1, float y1)//just do it my friend
{
	for (int i = 0; i < Len; i++)
	{
		mas[i].x += x1;
		mas[i].y += y1;
	}
	return *this;
}

Polygon & Polygon::operator+=(const point &A)
{

	if (!(this->vse_norm(A)))
		throw "incorrect point";

	point *mas1 = nullptr;

	try {
		mas1 = new point[Len + 1];
	}
	catch (std::bad_alloc& ba) {
		throw "out of memory";
	}

	for (int i = 0; i < Len; i++)
		mas1[i] = mas[i];

	mas1[Len++] = A;
	delete []mas;
	mas = mas1;

	

	return *this;
}


Polygon & Polygon::operator=(const Polygon& a)
{
	point *mas1;
	try {
		mas1 = new point[a.Len];
	}
	catch (std::bad_alloc& ba) {
		throw "out of memory";
	}
	Len = a.Len;

	for (int i = 0; i < Len; i++)
		mas1[i] = a.mas[i];
	
	mas = mas1;

	return *this;
}

Polygon & Polygon::operator=(Polygon&& a)//
{
	int azaza = Len;
	Len = a.Len;
	a.Len = azaza;

	point *tmp=mas;
	mas = a.mas;
	a.mas = tmp;
	
	return *this;
}//
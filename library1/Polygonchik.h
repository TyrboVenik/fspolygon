#pragma once
#include <iostream>
#define PI 3.14159265 

	struct point {
		float x;
		float y;
	};

	class Polygon {
	private:
		int Len;
		point *mas;
		point turnPoint(point&, const float &);
		int vse_norm(const point&)const;
	public:
		Polygon();
		Polygon(point);
		Polygon(float, float);
		Polygon(int, const point*);
		~Polygon();
		Polygon(Polygon&);
		Polygon(Polygon &&a);

		int getLen()const;
		point getCentre()const;
		Polygon&SetLen(int L);
		Polygon&SetPoint(int i, const point&);
		Polygon&SetPoint(int i, float, float);

		friend std::ostream& operator <<(std::ostream &, const Polygon&);
		point operator [](int)const;
		Polygon &operator()(int);
		Polygon &operator()(const point&);
		Polygon &operator()(float, float);
		Polygon &operator+=(const point&);
		Polygon &operator=(const Polygon&);
		Polygon &operator=(Polygon && a);
	};

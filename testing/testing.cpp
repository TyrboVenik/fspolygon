// testing.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\library1\Polygonchik.h"
#include "gtest\gtest.h"
#include <iostream>
#include <time.h>


using namespace std;

point *RandMas(int n, point arr[])
{
	float x;
	float y;
	int f;
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{
		f = 1;
		while (f) {
			f = 0;
			x = rand() % 10000 / 100;
			y = rand() % 10000 / 100;
			for (int j = 0; j < i; j++)
				if (arr[j].x == x && arr[j].y == y)
					f = 1;

		}
		arr[i].x = x;
		arr[i].y = y;
	}
	return arr;
}

TEST(Polygon_Testing, Check_Initialization)
{
	Polygon t1;

	EXPECT_DOUBLE_EQ(0, t1.getLen());

	Polygon t2(6, 7);
	point z1;
	z1.x = 6;
	z1.y = 7;
	EXPECT_DOUBLE_EQ(z1.x, t2[0].x);
	EXPECT_DOUBLE_EQ(z1.y, t2[0].y);

	Polygon t3(z1);
	EXPECT_DOUBLE_EQ(z1.x, t2[0].x);
	EXPECT_DOUBLE_EQ(z1.y, t2[0].y);

	point a1[100];
	RandMas(6, a1);
	EXPECT_NO_THROW(Polygon(6, a1), const char*);

	point a2[100];
	RandMas(2, a2);
	Polygon t4(2, a2);
	for (int i = 0; i < 2; i++)
	{
		EXPECT_DOUBLE_EQ(a2[i].y, t4[i].y);
		EXPECT_DOUBLE_EQ(a2[i].x, t4[i].x);
	}
};

TEST(Polygon_Testing, Chech_Centre)
{
	point a1[] = { 1,1,3,1,1,3,3,3 };
	Polygon t1(4, a1);
	point z1 = { 2,2 };
	EXPECT_DOUBLE_EQ(z1.x, t1.getCentre().x);
	EXPECT_DOUBLE_EQ(z1.y, t1.getCentre().y);


	Polygon t2(z1);
	EXPECT_DOUBLE_EQ(z1.x, t2.getCentre().x);
	EXPECT_DOUBLE_EQ(z1.y, t2.getCentre().y);

	Polygon t3;
	EXPECT_THROW(t3.getCentre(), const char*);
};

TEST(Polygon_Testing, Chech_Centre_New)
{
	float x1, y1, x2, y2;
	int l = 10, k = 0;
	srand(time(NULL));
	for (int i = 0; i < 10000; i++)
	{

		x1 = rand() % 100 - 50;
		y1 = rand() % 100 - 50;
		y2 = rand() % 100 - 50;
		if (y1<y2)
			y1 = y2;
		l = y1 - y2;
		if ((x1 > l / 2) && (y1 > l / 2))
			k++;
	}
	cout << "== == == == ==" << endl << k << endl << "== == == == ==" << endl;
};

TEST(Polygon_Testing, Chech_SetLen)
{
	point a1[100];
	RandMas(5, a1);
	Polygon t1(5, a1);

	for (int i = 5; i >= 0; i--)
	{
		EXPECT_NO_THROW(t1.SetLen(i), const char*);
		t1.SetLen(i);
		EXPECT_DOUBLE_EQ(t1.getLen(), i);
	}

	point a2[100];
	RandMas(3, a1);
	Polygon t2(3, a2);

	for (int i = 4; i < 10; i++)
		EXPECT_THROW(t2.SetLen(i), const char*);

	for (int i = -1; i > -10; i--)
		EXPECT_THROW(t2.SetLen(i), const char*);
}

TEST(Polygon_Testing, Chech_SetPoint)
{
	point a1[100];
	RandMas(5, a1);
	Polygon t1(5, a1);
	point z1;
	for (int i = 0; i < t1.getLen() - 1; i++)
	{
		z1.x = i;
		z1.y = -i;
		EXPECT_NO_THROW(t1.SetPoint(i, z1), const char*);
		t1.SetPoint(i, z1);
		EXPECT_NEAR(t1[i].x, z1.x, 0.0001);
		EXPECT_NEAR(t1[i].y, z1.y, 0.0001);
	}

	for (int i = 0; i < t1.getLen() - 1; i++)
	{
		EXPECT_NO_THROW(t1.SetPoint(i, i, -i), const char*);
		t1.SetPoint(i, i, -i);
		EXPECT_NEAR(t1[i].x, i, 0.001);
		EXPECT_NEAR(t1[i].y, -i, 0.001);
	}

	point a2[100];
	RandMas(2, a1);
	Polygon t2(2, a1);

	for (int i = -1; i > -5; i--)
		EXPECT_THROW(t2.SetPoint(i, i, -i), const char*);

	for (int i = 2; i < 10; i++)
		EXPECT_THROW(t2.SetPoint(i, i, -i), const char*);

	for (int i = -1; i > -5; i--)
	{
		z1.x = i;
		z1.y = -i;
		EXPECT_THROW(t2.SetPoint(i, z1), const char*);
	}

	for (int i = 2; i < 10; i++)
	{
		z1.x = i;
		z1.y = -i;
		EXPECT_THROW(t2.SetPoint(i, z1), const char*);
	}
}

TEST(Polygon_Operators_Testing, Chech_Add)
{
	Polygon t1;
	point a[100];
	RandMas(8, a);

	for (int i = 0; i < 5; i++)
	{
		EXPECT_NO_THROW(t1 += a[i]);
		EXPECT_DOUBLE_EQ(a[i].x, t1[i].x);
		EXPECT_DOUBLE_EQ(a[i].y, t1[i].y);
	}

	Polygon t2(5, 4);
	point z1 = { 5,4 };
	EXPECT_THROW(t2 += z1, const char*);

	/*Polygon o;
	for (int i = 0; 1 == 1; i++)
	{
	point b = { i,i };
	o += b;
	cout << i << endl;
	}*/
};

TEST(Polygon_Operators_Testing, Chech_Transfer)
{
	point a1[100];
	RandMas(5, a1);
	Polygon t1(5, a1);
	point z1 = { 4,9 };
	t1(z1);

	for (int i = 0; i < t1.getLen(); i++)
	{
		EXPECT_DOUBLE_EQ(a1[i].x + z1.x, t1[i].x);
		EXPECT_DOUBLE_EQ(a1[i].y + z1.y, t1[i].y);
	}

	point a2[100];
	RandMas(4, a1);
	Polygon t2(4, a2);
	point z2 = { -94,92 };

	t2(z2);
	for (int i = 0; i < t2.getLen(); i++)
	{
		EXPECT_DOUBLE_EQ(a2[i].x + z2.x, t2[i].x);
		EXPECT_DOUBLE_EQ(a2[i].y + z2.y, t2[i].y);
	}
};

TEST(Polygon_Operators_Testing, Chech_Turn)
{
	point a1[100];
	RandMas(5, a1);
	Polygon t1(5, a1);


	t1(90);
	for (int i = 0; i < t1.getLen(); i++)
	{

		EXPECT_NEAR(a1[i].y, -t1[i].x, 0.0001);
		EXPECT_NEAR(a1[i].x, t1[i].y, 0.0001);
	}

	point a2[100];
	RandMas(3, a2);
	Polygon t2(3, a2);

	t2(180);
	for (int i = 0; i < t2.getLen(); i++)
	{
		EXPECT_NEAR(a2[i].y, -t2[i].y, 0.0001);
		EXPECT_NEAR(a2[i].x, -t2[i].x, 0.0001);
	}

	point a3[100];
	RandMas(4, a3);
	Polygon t3(4, a3);


	t3(0);
	for (int i = 0; i < t3.getLen(); i++)
	{
		EXPECT_NEAR(a3[i].y, t3[i].y, 0.0001);
		EXPECT_NEAR(a3[i].x, t3[i].x, 0.0001);
	}

	EXPECT_THROW(t1(548), const char*);
	EXPECT_THROW(t1(-9657), const char*);
	EXPECT_THROW(t1(215), const char*);
	EXPECT_THROW(t1(54354), const char*);
	EXPECT_THROW(t1(35183), const char*);

	EXPECT_NO_THROW(t1(90 * 7), const char*);
	EXPECT_NO_THROW(t1(-90 * 5), const char*);
	EXPECT_NO_THROW(t1(90 * 13), const char*);
	EXPECT_NO_THROW(t1(0), const char*);
	EXPECT_NO_THROW(t1(8 * 90), const char*);
};


int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 1;
}

